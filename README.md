Moduł nadawczy CW (Morse) uniwersalnej gondoli stratosferycznej.
Pracuje jako urządzenie slave - czeka na komendę z komputera głównego z danymi do wysłania radiem. 
Modulacja AFSK, wymagane zewnętrzne radio (radiotelefon lub moduł radiowy).

Posiada możliwość przejścia w tryb samodzielnego trackera - po określonym czasie bez otrzymania komend z zewnątrz uruchamia własny gps i nadaje samodzielnie. 
Możliwe jest wymuszenie trybu samodzielnego przez odpowiedni rozkaz.

Język C, środowisko Atmel Studio 6.x, procesor Atmega644PA.

-- Wszystkie materiały udostępniane są na licencji open software/hardware do użytku niekomercyjnego

-- Uwaga - kompilacja w Atmel Studio 7+ uniemożliwi ponowne przeniesienie projektu do starszej wersji. 

Może też wystąpić błąd z funkcją init();. Należy używać jej wtedy jako INLINE.


---------------------------------------------------------------------------------
Schematy, pliki PCB i dokumentacja (częściowo nieaktualna): 

https://bitbucket.org/Dnuk/gondolav2-hardware-repo/overview


------------------------------------

Kontakt do nas oraz więcej materiałów tutaj:   https://www.facebook.com/DNFsystems/