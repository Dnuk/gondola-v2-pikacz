/*
 * main.c
 *
 *  Created on: 22-12-2013
 *      Author: Dann
 */


#include <avr/io.h> // standardowe wej/wyj
#include <util/delay.h> // op�nienia
#include <string.h> // operacje na stringach
#include <avr/interrupt.h> // przerwania
#include <stdlib.h> // zawiera funkcje itoa()
#include <avr/wdt.h> // watchdog
#include "MYUART/myuart.h"
//#include "TWI/mytwi.h" // I2C
#include <avr/power.h> // makra do oszcz. energii
//#include "math.h"

#define  wersja_softu "V09042016"

#define uart1_interrupt_enable UCSR1B |= (1<<RXCIE1)
#define uart1_interrupt_disable UCSR1B &= ~(1<<RXCIE1)

#define LED_ON     PORTC |= _BV(3)
#define LED_OFF    PORTC &= ~_BV(3)
#define PTT_ON     PORTA |= _BV(7)
#define PTT_OFF    PORTA &= ~_BV(7)
#define DTMF_IN    PINC & 0b11110000
#define GPS_SW_ON  PORTD |= _BV(6)
#define GPS_SW_OFF PORTD &= ~_BV(6)

#define buzzer_ON  power_timer0_enable()
#define buzzer_OFF power_timer0_disable()


volatile static uint16_t licznik = 0;
volatile static uint16_t prog_timer_sek = 0;
volatile static uint16_t prog_timer_sek_1 = 0;
volatile static uint8_t prog_timer_sek_2 = 0;
volatile static uint16_t pseudomillis = 0;
volatile static uint16_t milisek = 0;
volatile static uint16_t delay_timer = 0;

volatile static uint8_t flaga_modem_rx = 0;
volatile static uint8_t flaga_gps_rx0 = 0; // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
volatile static uint8_t flaga_gps_rx1 = 0; // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
volatile static uint8_t flaga_gps_rx2 = 0; // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

uint16_t mors_sekund = 240;

char String[83];
char String2[96];
char CzasUTC[7];
char Pozycja[30];
char Pozycja_Decym[20];
char Szerokosc[15];
char Dlugosc[15];
char Wysokosc[8];
char LastWysokosc[8]; // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
char Wznoszenie[4]; // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
char Predkosc[6];
volatile static uint8_t inc = 0;
volatile static uint8_t inc2 = 0;
volatile static char Sat[3];
char Fix[2];

uint8_t czasPodst = 39;   // PREDKOSC NADAWANIA MORSE'A - tak naprawde ilosc okresow generowanego dzwieku
                       	  // UWAGA! przy duzych szybkosciach wymagane wieksze wzmocnienie odbieranego sygna�u!
char bufor[10];
char bufor16[17];

uint16_t LicznikRamek = 1;

volatile static char IO_IN_OUT[5]; // {WE1, WE2, WY1, WY2}

volatile static uint8_t StandaloneMode = 0;
volatile static uint8_t StandaloneModeManual = 0;

void timer_buzzer (uint16_t czas);
void _delay_timer_ms(uint16_t czass);
void TX_ON();
void TX_OFF();
static inline void kreska();
static inline void kropka();
static inline void stopM();
static inline void stopD();
static inline void stopW();
void litera (char znak[]);
void slowo_tab (char tab[]);
void slowo_liczba (int liczba);
void RESET();
void RESET_Watchdog();
void TX_buzz_2 (uint8_t X, int Y, uint8_t Z, uint8_t n);
void telemetria_morse (char txt00[], char txt11[], char txt0[], char txt1[], char txt2[], char txt3[], char txt4[]);
void telemetria_morse_mini (char txt00[], char txt11[], char txt0[]);
uint16_t adc_pomiar ( uint8_t kanal);
void tele_slowo();

static inline void IntToString(int liczba);
static inline void LongToString(int32_t liczba);

void Konwersja_Pozycji();
void czysc_buf_wysokosc();

void init()
{
	/*
MCUSR = 0;						// neutralizuje watchdoga aby po jego uzyciu i resecie nie blokowal procka
WDTCSR = (1<<WDCE) | (1<<WDE);	// wazne aby to sie wykonalo jak najwczesniej po resecie (tutaj dziala)
WDTCSR = 0;

_delay_us(1); */

wdt_enable(WDTO_4S);  // od razu rekonfiguruje watchdoga'a na 4s i go resetuje dla pewnosci
wdt_reset();

DDRA = 	 0b11111111;	  // <<ADC>> uzywane kanaly jako wejcia!! - nieuzywane jako wyjscia
DDRB =   0b11111111;      // piny PB0, PB1 jako wyjscia PTT i audio MORSE
DDRC =   0b00001111;      // PC4-7 wejcia DTMF, PC3 wyjscie LED
DDRD =   0b11111111;       // wyjscia, PD6 GPS switch

PORTA =  0b11111111;    // port A
PORTB =  0b00000000;    // port B pocz. zera
PORTC =  0b00000000;    // port C pocz. zera
PORTD =  0b00000000;    // port D pocz. zera


uart0_init(__UBRR_0);		// inicjalizacja UART'u
uart1_init(__UBRR_1);


// TIMER2 W TRYBIE CTC
OCR2A = 14;  // 14745600/1024= >> 14400/14=1028.5Hz
TCCR2A |= (1<<WGM21); //tryb ctc
TCCR2B |= (1<<CS22) | (1<<CS21) | (1<<CS20);    // preskaler TIMER2 1024
TIMSK2 |= (1<<OCIE2A);

// TIMER0 W TRYBIE CTC - PRACA JAKO GENERATOR AUDIO
OCR0A = 7; // 147456Hz/1024=14400Hz >> 14400/9=2000hz
TCCR0A |= (1<<WGM01); // ctc
TCCR0B |= (1<<CS02) | (1<<CS00); // presc. 1024
TIMSK0 |= (1<<OCIE0A);

sei(); // globalne wlaczenie przerwan (cli() - wy��cza)

TX_ON();
_delay_timer_ms(100);
timer_buzzer(100);
_delay_timer_ms(70);
timer_buzzer(100);
TX_OFF();

_delay_timer_ms(100);
//uart1_puts(wersja_softu);
uart1_puts(">INIT OK");

prog_timer_sek=0;
}


ISR(USART0_RX_vect)  // OBSLUGA GPS >> UART0
{
uart1_interrupt_disable; // blokuje przerwania od portu komend   <<<<<<<<<<<<<

	unsigned char tmp = UDR0; // przepisz  znak z rejestru do zmiennej (nie, nie da sie bezposrednio)
	if (tmp != 10) { String[inc] = tmp; inc++; } // jesli odebrany znak jest rozny od <LF>=10 lub <CR>=13 to dopisz go do stringa
	else // jesli jednak wystapil taki znak, to znaczy ze skonczyl odbierac stringa
	{

		if ( String[3] == 'G' && String[4] == 'G' && String[5] == 'A' )		// przetwarzanie tych danych jeszcze w przerwaniu by je zabezpieczy� przed nadpisaniem
		{
			for(uint8_t i = 17; i <= 42; i++) Pozycja[i-17] = String[i];
			for(uint8_t i = 54; i <= 60 && String[i] != '.'; i++) Wysokosc[i-54] = String[i];
			for(uint8_t i = 7; i <= 12; i++) CzasUTC[i-7] = String[i];
			
			if (Fix[0] == '2' || Fix[0] == '3') { Sat[0] = String[46]; Sat[1] = String[47]; }
			else { Sat[0] = String[23]; Sat[1] = String[24]; }// 23 i 24 bez fixa
			flaga_gps_rx0 = 0xFF;	
		}

		else if ( String[3] == 'G' && String[4] == 'S' && String[5] == 'A' ) {Fix[0] = String[9]; flaga_gps_rx1 = 0xFF;}
		else if ( String[3] == 'R' && String[4] == 'M' && String[5] == 'C' ) {for(uint8_t i = 46; i <= 50; i++) Predkosc[i-46] = String[i]; flaga_gps_rx2 = 0xFF;}
				
		inc = 0;			//	na koniec zeruj zmienna inkrementujaca tablice stringa (inc)
		//flaga_gps_rx   // [kazdy if osobno] wywal flag� �e ma komplet danych (u�yte w kalkulacji np. pr�dko�ci wznoszenia) <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	}													

uart1_interrupt_enable; // odblokowuje przerwania od portu xbee    <<<<<<<<<<<<
}


ISR(USART1_RX_vect) // odbieranie komend >> UART1
{
	unsigned char tmp2 = UDR1;
	if (inc2 > 86) inc2 = 0;
	if ( (tmp2 == '>' && inc2 == 0) || (String2[0] == '>') )  //  <<<<<<<<<<<<<<<<<<
	{
	if (tmp2 != 13) { String2[inc2] = tmp2; inc2++; } // przepisz znaki do stringa dop�ki nie jest to <CR>
	else flaga_modem_rx = 1;						  // je�li jest, czyli sko�czy� zape�nia� string, to wywal flag�
	}
}


ISR (TIMER2_COMPA_vect) // obsluga przerwania od TIMER2
{
  licznik++;      // zmienna liczaca 1000 przerwan dla 1 sekundy
  pseudomillis++;  // zmienna liczaca czas do wyjscia z petli po '#'
  milisek++;
  delay_timer++;

  if (licznik >= 1028)// fizyczna liczba przerwan ktore uplyna zanim wykonasz dzialanie  // w tym wypadku ~1s
  	 {
	  	prog_timer_sek++;
	  	prog_timer_sek_1++;
	  	prog_timer_sek_2++;
	  	licznik = 0;
  	 }

}

ISR (TIMER0_COMPA_vect) // GENERATOR PRZEBIEGU 1kHz dla morse'a
{
		PORTA ^= _BV(4);       // neguj port co przerwanie
}


int main(void)
{
	init();  // inicjalizacja (wykonuje tylko raz)

	while(1) // GLOWNA PETLA PROGRAMU
	{
		wdt_reset();


		if (flaga_gps_rx0 == 0xFF && flaga_gps_rx1 == 0xFF && flaga_gps_rx2 == 0xFF) // sprawd� czy ma ka�d� ramk� z gps  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		{
			flaga_gps_rx0 = 0;		// skasuj flagi od wszystkich ramek
			flaga_gps_rx1 = 0;		//
			flaga_gps_rx2 = 0;		//
							
		}
		

//*** KOMENDY od Zarzadcy ************************************************************************************************ KOMENDY od Zarzadcy
		if (flaga_modem_rx == 1) // wykona to tylko gdy skompletowal komende odebrana z ziemi
		{
			uart1_interrupt_disable;  // blokuje odbieranie komend i innych znakow gdy trwa analiza poprzednich   

			flaga_modem_rx = 0;  // od razu kasuj flage na (wypadek niewykonania dalszej analizy)

			if ( strstr(String2, ">TX:") != 0 ) // gdy bufor zawiera dany ciag znakow
			{		
				for (int i=0; String2[i]!=0; i++) // przepusuje [przesuwa] string by wywalic tresc komendy a zostawic parametry
				{
					String2[i+1]=String2[i+4];
				}
				tele_slowo(String2); // pika pozostala tresc komunikatu
				prog_timer_sek_1 = 0; // zeruje licznik czasu braku reakcji (gdy nie przyszla zadna komenda)
			}
			else if ( strstr(String2, ">STM=1") != 0 && StandaloneModeManual != 0xFF )
			{
				StandaloneModeManual = 0xFF;
				uart1_puts(">ST-ON"); // nadaj komunikat o wlaczeniu trybu autonomicznego <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<,
			}
			else if ( strstr(String2, ">STM=0") != 0 && StandaloneModeManual != 0x00 )
			{
				StandaloneModeManual = 0x00;
				uart1_puts(">ST-OFF"); //    // nadaj komunikat o wylaczeniu trybu autonomicznego <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
			}		   		   
		else uart1_puts(">ERROR");


		inc2=0;  // zeruj licznik znakow po wykonaniu komendy

		for(uint8_t i = 0; i<=86; i++)  // czysc bufor komend
					{
						String2[i] = 0;
					}

			uart1_interrupt_enable;	//	odblokowuje odbieranie komend i innych znakow po analizie    
		}
		
//************************************************************************************************************************ KONIEC KOMEND od Zarzadcy


		if (prog_timer_sek_2 >= 1)	// wykonuje sie co 1s
		{
			PORTB ^= (1<<6); // neguj stan na pinie MOSI - generowanie zbocza narastajacego - reset zewnetrznego watchdoga
			prog_timer_sek_2 = 0;			
		}


//----------------------------------------------------------------------------------------------------------------------sprawdza tryb autonomiczny
		if (prog_timer_sek_1 >= 600 || StandaloneModeManual == 0xFF) // sprawdza czy minal krytyczny czas bez zadnej komendy od zarzadcy // 10min
		{
			StandaloneMode = 0xFF; // wlacz tryb autonomiczny [ten timer resetuje tylko przyjscie nowego rozkazu pikania]
		}
		else StandaloneMode = 0;

		if(StandaloneMode == 0xFF)
		{
			
			
			GPS_SW_ON; // wlacz zasilanie GPS
			
				if (prog_timer_sek >= mors_sekund)   // TELEMETRIA MORSEM CO ~240s = 4min
					{
						 if(Fix[0] == '2' || Fix[0] == '3')
							 {
								 Konwersja_Pozycji();
					 			 telemetria_morse_mini(Szerokosc, Dlugosc, Wysokosc);
					 			 czysc_buf_wysokosc();
							 }

					else if(Fix[0] == '1')
						{
							telemetria_morse_mini ("NO", "FIX", "/");					
						}

					prog_timer_sek = 0;
					}
		}
		else GPS_SW_OFF; // wylacz zasilanie  GPS		
//------------------------------------------------------------------------------------------------------------------- koniec sprawdzania trybu autonomicznego
		
		if ((DTMF_IN) == 0b00110000) // #    / oczekuj na #, potem wejdz w petle czekajaca na drugi znak
		{
				  pseudomillis=0;   // wyzeruj i przygotuj zmienna do liczenia czasu
			      while(1)
			      {
			    	  if(pseudomillis > 250) { pseudomillis=0; break; }	// czasowka wychodzaca z petli po czasie XXms

			         if ((DTMF_IN) == 0b10000000 ) { if(StandaloneMode == 0xFF) {Konwersja_Pozycji(); telemetria_morse_mini(Szerokosc, Dlugosc, Wysokosc);} else {uart1_puts(">D1"); break;}} // 1    >> telemetria morse
			    else if ((DTMF_IN) == 0b01000000 ) { uart1_puts(">D2"); break;}      // 2     >> reset glownego komputera
			    else if ((DTMF_IN) == 0b11000000 ) { uart1_puts(">D3"); RESET_Watchdog(); break; } // 3      => resetuj pikacza
			    else if ((DTMF_IN) == 0b00100000 ) { uart1_puts(">D4"); break;} // 4
			    else if ((DTMF_IN) == 0b10100000 ) { uart1_puts(">D5"); break;} // 5
			    else if ((DTMF_IN) == 0b01100000 ) { uart1_puts(">D6"); break;} // 6
			    else if ((DTMF_IN) == 0b11100000 ) { uart1_puts(">D7"); break;} // 7
			    else if ((DTMF_IN) == 0b00010000 ) { uart1_puts(">D8"); StandaloneModeManual = 0xFF;break;} // 8  // wlacz tryb standalone <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
			    else if ((DTMF_IN) == 0b10010000 ) { uart1_puts(">D9"); StandaloneModeManual = 0x00; break;} // 9 // wylacz tryb standalone <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
			    else if ((DTMF_IN) == 0b01010000 ) { uart1_puts(">D0"); break;}   // 0
			    else if ((DTMF_IN) == 0b11010000 ) { uart1_puts(">D*"); break;}   // *
			    else if ((DTMF_IN) == 0b10110000 ) { uart1_puts(">Da"); break;}   // A
			    else if ((DTMF_IN) == 0b01110000 ) { uart1_puts(">Db"); break;}   // B
			    else if ((DTMF_IN) == 0b11110000 ) { uart1_puts(">Dc"); break;}   // C
			      }
		}

	}
}


void timer_buzzer (uint16_t czas)
{
	milisek = 0;
	while(milisek < czas) buzzer_ON;
	if (milisek >= czas)
		{
			buzzer_OFF;
			milisek = 0;
		}
}

void TX_ON() // zalacza tranzystor (PTT)
{
	_delay_ms(50);
	uart1_puts(">R-ON");
	uart1_interrupt_disable; // wylacza obsluge przerwania USART_RX_vect  

 PTT_ON; //ptt on
 LED_ON; // dioda on
 _delay_timer_ms(150);
}

void TX_OFF() // wylacza tranzystor (PTT)
{
 _delay_ms(50);
 LED_OFF;  // dioda off
 PTT_OFF;  // ptt off
	
	uart1_interrupt_enable; // wlacza z powrotem obsluge przerwania USART_RX_vect
	_delay_us(1); // poczekaj na ponowne wlaczenie USART_RX_vect
	uart1_puts(">R-OFF");
	_delay_timer_ms(200); // op�nia wyjcie z funkcji (jedno z zabezp. przed blokowaniem I2C przez zak��cenia)
}

void _delay_timer_ms(uint16_t czass)
{
	delay_timer = 0;
	while(delay_timer <= czass);
}

static inline void kropka()  // generowanie jednej kropki
{
	timer_buzzer(czasPodst);
}

static inline void kreska()  // generowanie jednej kreski
{
	timer_buzzer(czasPodst*3);
}

static inline void stopM()
{
	_delay_timer_ms(czasPodst);  // przerwa czasowa mi�dzy kropkami i kreskami
}

static inline void stopD()
{
	_delay_timer_ms(czasPodst*3); // przerwa czasowa mi�dzy pojedynczymi literami w s�owie
}

static inline void stopW()
{
	_delay_timer_ms(czasPodst*7); // przerwa czasowa mi�dzy s�owami
}

void litera(char znak[])
{
	wdt_reset();		// resetuje watchdoga co litere, bo slowo moze trwac nawet kilkanascie sek wiec za dlugo
						// litera trwa max 1-2s
	stopD();
	for (int i = 0; i < strlen(znak); i++)
    {
		if (znak[i] == '.'){ kropka(); if(i < strlen(znak)-1) stopM();}
		else if (znak[i] == '-'){ kreska(); if(i < strlen(znak)-1) stopM();}
    }
}

void slowo_tab (char tab[])
{
   stopW();
   for(int i = 0; i < strlen(tab); i++)
   {
		switch(tab[i])
		{
                     case 'A': litera(".-"); break;
                     case 'B': litera("-..."); break;
                     case 'C': litera("-.-."); break;
                     case 'D': litera("-.."); break;
                     case 'E': litera("."); break;
                     case 'F': litera("..-."); break;
                     case 'G': litera("--."); break;
                     case 'H': litera("...."); break;
                     case 'I': litera(".."); break;
                     case 'J': litera(".---"); break;
                     case 'K': litera("-.-"); break;
                     case 'L': litera(".-.."); break;
                     case 'M': litera("--"); break;
                     case 'N': litera("-."); break;
                     case 'O': litera("---"); break;
                     case 'P': litera(".--."); break;
                     case 'Q': litera("--.-"); break;
                     case 'R': litera(".-."); break;
                     case 'S': litera("..."); break;
                     case 'T': litera("-"); break;
                     case 'U': litera("..-"); break;
                     case 'V': litera("...-"); break;
                     case 'W': litera(".--"); break;
                     case 'X': litera("-..-"); break;
                     case 'Y': litera("-.--"); break;
                     case 'Z': litera("--.."); break;
                     case '1': litera(".----"); break;
                     case '2': litera("..---"); break;
                     case '3': litera("...--"); break;
                     case '4': litera("....-"); break;
                     case '5': litera("....."); break;
                     case '6': litera("-...."); break;
                     case '7': litera("--..."); break;
                     case '8': litera("---.."); break;
                     case '9': litera("----."); break;
                     case '0': litera("-----"); break;
                     case ' ': stopD(); break;
                     //case '+': litera(".-.-."); break;  // znak konca transmisji
                     case '-': litera("-....-"); break;
                     case '/': litera("-..-."); break;
                     case '.': litera(".-.-.-"); break;
                     case ',': litera("--..--"); break;
                     //case '*': litera(".----."); break; // apostrof (')
                     case '@': litera(".--.-."); break;
                     case ':': litera("---..."); break;
                     //case ';': litera("-.-.-."); break;
                     //case '=': litera("-...-"); break;
                     //case '"': litera(".-..-."); break;
                     //case '$': litera("...-..-"); break;
                     //case '_': litera("..--.-"); break;
                     //case '?': litera("..--.."); break;
                     //case '!': litera("-.-.--"); break;
                     //case '(': litera("-.--."); break;
                     //case ')': litera("-.--.-"); break;
		}
	}
}

 void slowo_liczba (int liczba) // liczby max do 65'535
{
   char bufor[10];
   itoa(liczba, bufor, 10);
   slowo_tab(bufor);          // wywo�anie funkcji "s�owo" z parametrem b�d�cym przekonwertowan� warto�ci� zmiennej
}

void TX_buzz_2 (uint8_t X, int Y, uint8_t Z, uint8_t n)
{
	TX_ON();
	//buzzer(X,Y,Z,n);
	TX_OFF();
}

void RESET() // funkcja resetowania CPU przez skok do poczatku pamieci bootloadera
{
	cli();				  // deaktywuje przerwania zewnetrzne
	_delay_timer_ms(100); // poczekaj troszke
	asm("jmp 0x7800;");	  // skocz do poczatku pamieci bootloadera (przy 2kb pojemnosci)
}

void RESET_Watchdog() // <<<<<<<<<<< !!!!!!!!!!!!!!!
{
	   _delay_ms(1000);
	   TX_ON();
	   timer_buzzer(1500);
	   _delay_timer_ms(100);
	   TX_OFF();
	   wdt_enable(WDTO_30MS);
	   cli();
	   while(1);
}


void telemetria_morse (char txt00[], char txt11[], char txt0[], char txt1[], char txt2[], char txt3[], char txt4[])
{
	TX_ON();
	_delay_timer_ms(500);
	slowo_tab(" /TELE-");
	slowo_tab(txt00);
	slowo_tab(txt11);
	slowo_tab(txt0);
	slowo_tab(txt1);
	slowo_tab(txt2);
	slowo_tab(txt3);
	slowo_tab(txt4);
	slowo_tab("/");
	TX_OFF();
}

void telemetria_morse_mini (char txt00[], char txt11[], char txt0[])
{
	TX_ON();
	_delay_timer_ms(500);
	slowo_tab(" /STLE-");
	slowo_tab(txt00);
	slowo_tab(txt11);
	slowo_tab(txt0);
	slowo_tab("/");
	TX_OFF();
}

void tele_slowo (char txt[])
{
	TX_ON();
	_delay_ms(500);
	slowo_tab(txt);
	TX_OFF();
}



static inline void IntToString(int liczba)
{
	itoa(liczba, bufor, 10);
}

static inline void LongToString(int32_t liczba)
{
	ltoa(liczba, bufor16, 10);
}


void Konwersja_Pozycji()	// zamiana pozycji gps z formatu protokolu NMEA na format dziesietny
{	
	char Szer_pocz[4];
	char Dlug_pocz[4];
	double Szerokosc_liczba = 0;
	double Dlugosc_liczba = 0;
	double Szer_liczba_pocz = 0;
	double Dlug_liczba_pocz = 0;
	
	for(uint8_t i = 0; i <= 7; i++) Szerokosc[i] = Pozycja[i+2]; // przepisz z pozycji tylko dalsza czesc szerokosci geo
	for(uint8_t i = 0; i <= 7; i++) Dlugosc[i] = Pozycja[i+16];  // przepisz sama dalsza czesc dlugosci geo
	Szer_pocz[0]=Pozycja[0]; Szer_pocz[1]=Pozycja[1];			// przepisz tylko stopnie szerokosci
	Dlug_pocz[0]=Pozycja[13]; Dlug_pocz[1]=Pozycja[14];	Dlug_pocz[2]=Pozycja[15]; // przepisz tylko stopnie dlugosci

	Szer_liczba_pocz = atof(Szer_pocz);			// konwersja stringow na liczby zmiennoprzecinkowe
	Dlug_liczba_pocz = atof(Dlug_pocz);			//
	Szerokosc_liczba = atof(Szerokosc);			//
	Dlugosc_liczba = atof(Dlugosc);				//

	Szerokosc_liczba = Szerokosc_liczba/60.0 + Szer_liczba_pocz; // przeliczenie na pozycje w formacie decymentalnym
	Dlugosc_liczba = Dlugosc_liczba/60.0 + Dlug_liczba_pocz;

	dtostrf(Szerokosc_liczba,0,5,Szerokosc);		// konwersja spowrotem do stringa
	dtostrf(Dlugosc_liczba,0,5,Dlugosc);			// dla wygody uzyto zmiennych z poczatku funkcji [szerokosc i dlugosc]

	//Szerokosc[strlen(Szerokosc)] = Pozycja[11]; // dodanie znaczka kierunku geograficznego "N,S,E,W"
	//Dlugosc[strlen(Dlugosc)] = Pozycja[25];
}

void czysc_buf_wysokosc()
{
	for(uint8_t i = 0; i<=8; i++)
	{
		Wysokosc[i] = 0;
	}
}



